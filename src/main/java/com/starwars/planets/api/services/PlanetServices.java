package com.starwars.planets.api.services;

import java.util.List;

import org.springframework.validation.BindingResult;

import com.starwars.planets.api.domain.Planet;
import com.starwars.planets.api.responses.Response;

public interface PlanetServices {
	
	Response<Planet> registerPlanet(Planet planet, BindingResult bindingResult);

	Response<List<Planet>> findPlanets();

	Response<Planet> findPlanetById(String id);

	Response<Planet> findPlanetByName(String name);

	Response<String> removePlanetById(String id);

}

