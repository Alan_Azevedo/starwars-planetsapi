package com.starwars.planets.api.services;

import java.util.Arrays;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.starwars.planets.api.domain.Planet;
import com.starwars.planets.api.domain.PlanetSWApi;
import com.starwars.planets.api.domain.PlanetSWApiResponse;
import com.starwars.planets.api.repositories.PlanetRepository;
import com.starwars.planets.api.responses.Response;
import com.starwars.planets.api.services.exception.BusinessException;
import com.starwars.planets.api.services.exception.IntegrationException;
import com.starwars.planets.api.services.exception.ObjectNotFoundException;
import com.starwars.planets.api.services.exception.ServiceUnavailableException;

@Service
public class PlanetServicesImpl implements PlanetServices{
	
	private final MongoTemplate mongoTemplate;
	
	@Autowired
    public PlanetServicesImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

	@Autowired
	private PlanetRepository planetRepository;
	
	
	/**
	 * Method to register planet
	 * @author Álan Azevedo
	 * @return Planet.
	 */
	@Override
	public Response<Planet> registerPlanet(Planet planet, BindingResult bindingResult) {
		validate(planet, bindingResult);
		try {			
			Response<PlanetSWApi> planetSWApiResponse = consultServiceSW(planet);
			planet.setNumberAppearances(planetSWApiResponse.getResult().getFilms().size());
			Planet regitedPlanet = this.planetRepository.save(planet);
			return new Response<Planet>(regitedPlanet);
		} catch (BusinessException be) {        	
        	throw new BusinessException(be.getMessage());	
        } catch (Exception e) {        	
        	throw new IntegrationException("Ocorreu um erro ao tentar registar o planeta: " + planet.getName());
        } 
	}	
	
	/**
	 * Method to list of planets
	 * @author Álan Azevedo
	 * @return List<Planet>.
	 */
	@Override
	public Response<List<Planet>> findPlanets() {
		try {
			return new Response<List<Planet>>(this.planetRepository.findAll());
		} catch (Exception e) {        	
	    	throw new IntegrationException("Ocorreu um erro ao tentar listar os planetas.");
	    }
	}
	
	/**
	 * Method to find planet by id
	 * @author Álan Azevedo
	 * @param String - id of planet
	 * @return Planet.
	 */
	@Override
	public Response<Planet> findPlanetById(String id) {	
		try {
			Planet planet = this.planetRepository.findById(id).orElse(null);
			if(null != planet) {
				return new Response<Planet>(planet);
			} else {
				throw new ObjectNotFoundException("Objeto não encontrado, Id: " + id);
			}
		} catch (Exception e) {        	
	    	throw new IntegrationException("Ocorreu um erro ao tentar consultar o planeta por id: " + id);
	    }
	}
	
	/**
	 * Method to find planet by name
	 * @author Álan Azevedo
	 * @param String - name of planet
	 * @return Planet.
	 */
	@Override
	public Response<Planet> findPlanetByName(String name) {
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("name").is(name));
			Planet planet = this.mongoTemplate.findOne(query, Planet.class);
			if(null != planet) {
				return new Response<Planet>(planet);
			} else {
				throw new ObjectNotFoundException("Objeto não encontrado, nome: " + name);
			}
		} catch (ObjectNotFoundException onfe) {        	
        	throw new ObjectNotFoundException(onfe.getMessage());
        } catch (Exception e) {        	
        	throw new IntegrationException("Ocorreu um erro ao tentar consultar o planeta por nome: " + name);
        } 		
	}
	
	/**
	 * Method to remove planet by id
	 * @author Álan Azevedo
	 * @param String - id of planet
	 * @return String.
	 */
	@Override
	public Response<String> removePlanetById(String id) {
		try {
			this.planetRepository.deleteById(id);
			return new Response<String>("Planeta removido.");
		} catch (Exception e) {        	
        	throw new IntegrationException("Ocorreu um erro ao tentar remover o planeta Id: " + id);
        } 
			
	}
	
	public Response<PlanetSWApi> consultServiceSW(Planet planet) {
		RestTemplate restTemplate = new RestTemplate();		
		HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<?> entity = new HttpEntity<>(headers);

        String url = "https://swapi.co/api/planets/?search="+planet.getName();
        try {
        	ResponseEntity<PlanetSWApiResponse> planetSWApiResponse = restTemplate.exchange(url, HttpMethod.GET, entity, PlanetSWApiResponse.class);
        	if(200 == planetSWApiResponse.getStatusCodeValue() && !"0".equals(planetSWApiResponse.getBody().getCount())) {
    	        List<PlanetSWApi> listPlanetSWApi = planetSWApiResponse.getBody().getResults();
    	        PlanetSWApi planetSWApi = new PlanetSWApi();
    	        if(listPlanetSWApi.size() == 1){
    	        	planetSWApi = listPlanetSWApi.get(0);
    	        } else {
    	        	throw new BusinessException("Foram encontrados "+listPlanetSWApi.size()+" planetas com o nome '" + planet.getName() + "'. Favor enviar o nome completo do planeta para cadastro.");
    	        }  	            	        
    	        return new Response<PlanetSWApi>(planetSWApi);
        	} else {
        		throw new ObjectNotFoundException("Planeta não encontrado no serviço SWAPI: " + planet.getName());
        	}
        } catch (BusinessException be) {        	
        	throw new BusinessException(be.getMessage());	
        } catch (RestClientException rce) {        	
        	throw new ServiceUnavailableException("Serviço SWAPI indisponível, por favor tente novamente em instantes.");	
        } catch (Exception e) {        	
        	throw new IntegrationException("Ocorreu um erro ao tentar consultar o serviço SWAPI.");
        }        
	}
	
	public void validate(Planet planet, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			StringBuffer msg = new StringBuffer();
			for (ObjectError erro : bindingResult.getAllErrors()) {
				msg.append(erro.getDefaultMessage()+" ");
			}
			throw new BusinessException(msg.toString());
		}
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("name").is(planet.getName()));
			Planet result = this.mongoTemplate.findOne(query, Planet.class);
			if(null != result) {
				throw new BusinessException("Planeta já cadastrado.");
			}
		} catch (BusinessException be) {        	
        	throw new BusinessException(be.getMessage());
        } catch (Exception e) {        	
        	throw new IntegrationException("Ocorreu um erro ao tentar consultar o planeta por nome: " + planet.getName());
        } 	
	}
	
}

