package com.starwars.planets.api.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PlanetSWApiResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("count")
    private String count;
	@JsonProperty("next")
    private String next;
	@JsonProperty("previous")
    private String previous;
    @JsonProperty("results")
    private List<PlanetSWApi> results;
    
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public String getNext() {
		return next;
	}
	public void setNext(String next) {
		this.next = next;
	}
	public String getPrevious() {
		return previous;
	}
	public void setPrevious(String previous) {
		this.previous = previous;
	}
	public List<PlanetSWApi> getResults() {
		return results;
	}
	public void setResults(List<PlanetSWApi> results) {
		this.results = results;
	}

}

