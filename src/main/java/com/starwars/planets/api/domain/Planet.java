package com.starwars.planets.api.domain;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;



@Document
public class Planet implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	@NotEmpty(message = "Favor informar o nome.")
	private String name;
	@NotEmpty(message = "Favor informar o clima.")
	private String climate;
	@NotEmpty(message = "Favor informar o terreno.")
	private String terrain;
	private int numberAppearances;
	
	public Planet() {
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClimate() {
		return climate;
	}

	public void setClimate(String climate) {
		this.climate = climate;
	}

	public String getTerrain() {
		return terrain;
	}

	public void setTerrain(String terrain) {
		this.terrain = terrain;
	}
	
	public int getNumberAppearances() {
		return numberAppearances;
	}

	public void setNumberAppearances(int numberAppearances) {
		this.numberAppearances = numberAppearances;
	}

	public Planet id(String id) {
		setId(id);
		return this;
	}
	public Planet name(String name) {
		setName(name);
		return this;
	}
	public Planet climate(String climate) {
		setClimate(climate);
		return this;
	}
	public Planet terrain(String terrain) {
		setTerrain(terrain);
		return this;
	}
	public Planet numberAppearances(int numberAppearances) {
		setNumberAppearances(numberAppearances);
		return this;
	}

}

