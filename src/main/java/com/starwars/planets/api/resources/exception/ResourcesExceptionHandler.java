package com.starwars.planets.api.resources.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.starwars.planets.api.services.exception.ObjectNotFoundException;
import com.starwars.planets.api.services.exception.ServiceUnavailableException;
import com.starwars.planets.api.services.exception.IntegrationException;
import com.starwars.planets.api.services.exception.BusinessException;


@ControllerAdvice
public class ResourcesExceptionHandler {
	
	@ExceptionHandler(ObjectNotFoundException.class)
	public ResponseEntity<StandardError> objectNotFound(ObjectNotFoundException exception, HttpServletRequest request){
		StandardError erro = new StandardError(HttpStatus.NOT_FOUND.value(), exception.getMessage(), System.currentTimeMillis());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
	}
	
	@ExceptionHandler(ServiceUnavailableException.class)
	public ResponseEntity<StandardError> serviceUnavailable(ServiceUnavailableException exception, HttpServletRequest request){
		StandardError erro = new StandardError(HttpStatus.SERVICE_UNAVAILABLE.value(), exception.getMessage(), System.currentTimeMillis());
		return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(erro);
	}
	
	@ExceptionHandler(IntegrationException.class)
	public ResponseEntity<StandardError> IntegrationException(IntegrationException exception, HttpServletRequest request){
		StandardError erro = new StandardError(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getMessage(), System.currentTimeMillis());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(erro);
	}
	
	@ExceptionHandler(BusinessException.class)
	public ResponseEntity<StandardError> BusinessException(BusinessException exception, HttpServletRequest request){
		StandardError erro = new StandardError(HttpStatus.BAD_REQUEST.value(), exception.getMessage(), System.currentTimeMillis());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(erro);
	}

}
