package com.starwars.planets.api.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.starwars.planets.api.domain.Planet;
import com.starwars.planets.api.responses.Response;
import com.starwars.planets.api.services.PlanetServices;

@RestController
@RequestMapping(path = "/api/planet")
public class PlanetResource {
	
	@Autowired
	private PlanetServices planetServices;
	
	@PostMapping
	public ResponseEntity<?> registerPlanet(@Valid @RequestBody Planet planet, BindingResult bindingResult){
		Response<Planet> response = this.planetServices.registerPlanet(planet, bindingResult);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
	
	@GetMapping
	public ResponseEntity<?> findPlanets(){
		Response<List<Planet>> response = this.planetServices.findPlanets();
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findPlanetById(@PathVariable(name = "id") String id){		
		Response<Planet> response = this.planetServices.findPlanetById(id);
		return ResponseEntity.ok(response);			
	}
	
	@GetMapping("/findname={name}")
	public ResponseEntity<?> findPlanetByName(@PathVariable(name = "name") String name){
		Response<Planet> response = this.planetServices.findPlanetByName(name);
		return ResponseEntity.ok(response);			
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> removePlanet(@PathVariable(name = "id") String id){
		Response<String> response = this.planetServices.removePlanetById(id);
		return ResponseEntity.ok(response);
	}
	

}
