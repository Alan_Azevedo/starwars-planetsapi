package com.starwars.planets.api.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.starwars.planets.api.domain.Planet;

public interface PlanetRepository extends MongoRepository<Planet, String> {

}