Chamadas aos serviços:

Registar planeta 
POST >> http://localhost:8080/api/planet
body 
{
	"name": "Alderaan",
	"climate": "temperate",
	"terrain": "grasslands, mountains"
}
POST >> http://localhost:8080/api/planet
body 
{
	"name": "Dagobah",
	"climate": "murky",
	"terrain": "swamp, jungles"
}

--- Listar planeta:
    GET >> http://localhost:8080/api/planet

--- Buscar planeta por id:
    GET >> http://localhost:8080/api/planet/{ID_PLANETA}
	
--- Buscar planeta por nome:
    GET >> http://localhost:8080/api/planet/findname={NOME_PLANETA}

--- Remover planeta:
    DELETE >> http://localhost:8080/api/planet/{ID_PLANETA}